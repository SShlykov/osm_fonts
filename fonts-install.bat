rem Windows font installation utility. Must be run as administrator
copy "Dejavu Sans\*.ttf" %systemroot%\fonts
copy "Hanazono\*.ttf" %systemroot%\fonts
copy "Noto Sans\*.ttf" %systemroot%\fonts
copy "Noto Sans\*.otf" %systemroot%\fonts
copy "Unifont\*.ttf" %systemroot%\fonts
regedit /s fonts.reg